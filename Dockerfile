FROM ubuntu:16.04
EXPOSE 10000
MAINTAINER Michal Belica <devel@beli.sk>

ENV GOSU_VERSION 1.10
RUN set -ex; \
	fetchDeps=' \
		ca-certificates \
		wget \
	'; \
	apt-get update; \
	apt-get install -y --no-install-recommends $fetchDeps; \
	apt-get clean; \
	rm -rf /var/lib/apt/lists/*; \
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
# verify the signature
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
	rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc; \
	chmod +x /usr/local/bin/gosu; \
# verify that the binary works
	gosu nobody true; \
	apt-get purge -y --auto-remove $fetchDeps

RUN apt-get update && apt-get install -y firefox xpra && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN sed -ie 's/^start-child/#start-child/' /etc/xpra/xpra.conf
RUN useradd -d /home/user -m user
ADD https://downloads.getmonero.org/gui/linux64 /opt
RUN mv /opt/monero* /opt/monero && chown -R root: /opt/monero \
	&& find /opt/monero -type f -exec chmod 0644 '{}' + \
	&& find /opt/monero -type d -exec chmod 0755 '{}' + \
	&& chmod 0755 /opt/monero/monero*
RUN mkdir -p /Monero/config && chown user: -R /Monero
USER user
ENV HOME /home/user
WORKDIR /home/user
RUN ln -s /Monero Monero && mkdir .config && ln -s /Monero/config .config/monero-project

ENV LD_LIBRARY_PATH=/opt/monero/libs
ENV QT_PLUGIN_PATH=/opt/monero/plugins
ENV QML2_IMPORT_PATH=/opt/monero/qml

USER root
VOLUME /Monero
ADD entrypoint /entrypoint
RUN chmod a+x /entrypoint

CMD ["/entrypoint"]
